package com.wikitude.samples;

/**
 * Created by ralexand on 1/22/2017.
 */

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.wikitude.sdksamples.R;

public class AlphabetListFragment extends ListFragment {
    String[] alphabet = new String[] {"MLXe_8_Slot_60a","BR-SLX9850-4","ICX1"};

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup viewGroup, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.menu_fragment, viewGroup, false);
        ArrayAdapter adapter = new ArrayAdapter(getActivity(),
                android.R.layout.simple_list_item_activated_1, alphabet);
        setListAdapter(adapter);

        return view;
    }
    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        OutputFragment txt = (OutputFragment)getFragmentManager().findFragmentById(R.id.output);
        //txt.display(word[position]);
        getListView().setSelector(android.R.color.holo_red_dark);
    }
}
