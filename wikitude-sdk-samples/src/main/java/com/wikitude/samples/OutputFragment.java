package com.wikitude.samples;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wikitude.architect.ArchitectView;
import com.wikitude.architect.StartupConfiguration;
import com.wikitude.sdksamples.R;
import com.wikitude.tools.device.features.MissingDeviceFeatures;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by ralexand on 1/22/2017.
 */

public class OutputFragment extends Fragment implements View.OnClickListener {
    private Map<Integer, List<MainActivity.SampleMeta>> samples;

    private Set<String> irSamples;
    private Set<String> geoSamples;
    private Button button;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.output_fragment, viewGroup, false);

        button = (Button) view.findViewById(R.id.word);
        button.setOnClickListener(this);
        button.setText("Launch AR");

        irSamples = getListFrom("samples/samples_ir.lst");
        geoSamples = getListFrom("samples/samples_geo.lst");
        final String[] values = this.getListLabels();

        return view;
    }

    @Override
    public void onClick(View v) {
        startMainActivity(0);
    }


    private Set<String> getListFrom(String fname) {
        HashSet<String> data = new HashSet<String>();
        try {
            BufferedReader burr = new BufferedReader(new InputStreamReader(getActivity().getAssets().open(fname)));
            String line;
            while ((line = burr.readLine()) != null) {
                data.add(line);
            }
            burr.close();
        } catch (FileNotFoundException e) {
            Log.w("Wikitude SDK Samples", "Can't read list from file " + fname);
        } catch (IOException e) {
            Log.w("Wikitude SDK Samples", "Can't read list from file " + fname);
        }
        return data;
    }

//    @Override
//    protected void onListItemClick(ListView l, View v, int position, long id) {
//        super.onListItemClick(l, v, position, id);
//        startMainActivity(position);
//    }

    private void startMainActivity(int position) {
        final Intent intent = new Intent(getActivity(), MainSamplesListActivity.class);

        final List<MainActivity.SampleMeta> activitiesToLaunch = getActivitiesToLaunch(position);
        final String activityTitle = activitiesToLaunch.get(0).categoryId + ". " + activitiesToLaunch.get(0).categoryName.replace("$", " ");
        String[] activityTitles = new String[activitiesToLaunch.size()];
        String[] activityUrls = new String[activitiesToLaunch.size()];
        String[] activityClasses = new String[activitiesToLaunch.size()];

        boolean[] activitiesIr = new boolean[activitiesToLaunch.size()];
        boolean[] activitiesGeo = new boolean[activitiesToLaunch.size()];

        // check if AR.VideoDrawables are supported on the current device. if not -> show hint-Toast message
        if (activitiesToLaunch.get(0).categoryName.contains("Video") && !MainActivity.isVideoDrawablesSupported()) {
            Toast.makeText(getActivity(), R.string.videosrawables_fallback, Toast.LENGTH_LONG).show();
        }

        // find out which Activity to launch when sample row was pressed, some handle document.location = architectsdk:// events, others inject poi data from native via javascript
        for (int i = 0; i < activitiesToLaunch.size(); i++) {
            final MainActivity.SampleMeta meta = activitiesToLaunch.get(i);

            activityTitles[i] = (meta.categoryId + "." + meta.sampleId + " " + meta.sampleName.replace("$", " "));
            activityUrls[i] = meta.path;
            activitiesIr[i] = meta.hasIr;
            activitiesGeo[i] = meta.hasGeo;

            if (meta.categoryId.equals("5") && meta.sampleId == 1) {
                activityClasses[i] = ("com.wikitude.samples.SampleCamContentFromNativeActivity");
            } else if (meta.categoryId.equals("8") && meta.sampleId == 1) {
                activityClasses[i] = ("com.wikitude.samples.SampleFrontCamActivity");
            } else if (meta.categoryId.equals("9") && meta.sampleId == 1) {
                activityClasses[i] = ("com.wikitude.samples.SamplePluginActivity");
            } else if (meta.categoryId.equals("9") && meta.sampleId == 2) {
                activityClasses[i] = ("com.wikitude.samples.FaceDetectionPluginActivity");
            } else if (meta.categoryId.equals("9") && meta.sampleId == 3) {
                activityClasses[i] = ("com.wikitude.samples.CustomCameraActivity");
            } else if (meta.categoryId.equals("9") && meta.sampleId == 4) {
                activityClasses[i] = ("com.wikitude.samples.MarkerTrackingPluginActivity");
            } else {
                activityClasses[i] = ("com.wikitude.samples.SampleCamActivity");
            }
        }

        intent.putExtra(MainSamplesListActivity.EXTRAS_KEY_ACTIVITIES_ARCHITECT_WORLD_URLS_ARRAY, activityUrls);
        intent.putExtra(MainSamplesListActivity.EXTRAS_KEY_ACTIVITIES_CLASSNAMES_ARRAY, activityClasses);
        intent.putExtra(MainSamplesListActivity.EXTRAS_KEY_ACTIVITIES_TILES_ARRAY, activityTitles);
        intent.putExtra(MainSamplesListActivity.EXTRAS_KEY_ACTIVITIES_IR_ARRAY, activitiesIr);
        intent.putExtra(MainSamplesListActivity.EXTRAS_KEY_ACTIVITIES_GEO_ARRAY, activitiesGeo);
        intent.putExtra(MainSamplesListActivity.EXTRAS_KEY_ACTIVITY_TITLE_STRING, activityTitle);

			/* launch activity */
        this.startActivity(intent);
    }

    protected final String[] getListLabels() {
        boolean includeIR = (ArchitectView.getSupportedFeaturesForDevice(getActivity().getApplicationContext()) & StartupConfiguration.Features.Tracking2D) != 0;
        boolean includeGeo = (ArchitectView.getSupportedFeaturesForDevice(getActivity().getApplicationContext()) & StartupConfiguration.Features.Geo) != 0;

        MissingDeviceFeatures missingDeviceFeatures = ArchitectView.isDeviceSupported(getActivity(), StartupConfiguration.Features.Tracking2D | StartupConfiguration.Features.Geo);

        if (missingDeviceFeatures.areFeaturesMissing()) {
            Toast toast = Toast.makeText(getActivity(), missingDeviceFeatures.getMissingFeatureMessage() +
                    "Because of this some samples may not be visible.", Toast.LENGTH_LONG);
            toast.show();
        }

        samples = getActivitiesToLaunch(includeIR, includeGeo);
        final String[] labels = new String[samples.keySet().size()];
        for (int i = 0; i < labels.length; i++) {
            labels[i] = samples.get(i).get(0).categoryId + ". " + samples.get(i).get(0).categoryName.replace("$", " ");
        }
        return labels;
    }

    protected int getContentViewId() {
        return R.layout.list_startscreen;
    }

    public void buttonClicked(final View view) {
        try {
            this.startActivity(new Intent(getActivity(), Class.forName("com.wikitude.samples.utils.urllauncher.ARchitectUrlLauncherActivity")));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    private List<MainActivity.SampleMeta> getActivitiesToLaunch(final int position) {
        return samples.get(position);
    }

    private Map<Integer, List<MainActivity.SampleMeta>> getActivitiesToLaunch(boolean includeIR, boolean includeGeo) {
        final Map<Integer, List<MainActivity.SampleMeta>> pos2activites = new HashMap<Integer, List<MainActivity.SampleMeta>>();

        String[] assetsIWant;

        try {
            assetsIWant = getActivity().getAssets().list("samples");
            int pos = -1;
            String lastCategoryId = "";
            for (final String asset : assetsIWant) {
                if (!asset.substring(asset.length() - 4).contains(".")) {
                    try {
                        // don't include sample if it requires IR functionality on
                        // devices which don't support it.
                        boolean needIr = irSamples.contains(asset);
                        boolean needGeo = geoSamples.contains(asset);
                        if ((includeIR || !needIr) && (includeGeo || !needGeo)) {
                            MainActivity.SampleMeta sampleMeta = new MainActivity.SampleMeta(asset, needIr, needGeo);
                            if (!sampleMeta.categoryId.equals(lastCategoryId)) {
                                pos++;
                                pos2activites.put(pos, new ArrayList<MainActivity.SampleMeta>());
                            }
                            pos2activites.get(pos).add(sampleMeta);
                            lastCategoryId = sampleMeta.categoryId;
                        }
                    } catch (IllegalArgumentException e) {
                        // Log.e("Ignored Asset to load", asset + " invalid: "+ e.getMessage());
                    }
                }
            }

            return pos2activites;


        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
