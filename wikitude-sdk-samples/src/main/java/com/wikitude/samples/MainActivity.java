package com.wikitude.samples;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.app.ListActivity;
import android.content.Intent;
import android.opengl.GLES20;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wikitude.architect.ArchitectView;
import com.wikitude.architect.StartupConfiguration;
import com.wikitude.sdksamples.R;
import com.wikitude.tools.device.features.MissingDeviceFeatures;


/**
 * Activity launched when pressing app-icon.
 * It uses very basic ListAdapter for UI representation
 */
public class MainActivity extends ListActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // ensure to clean cache when it is no longer required
        MainActivity.deleteDirectoryContent(ArchitectView.getCacheDirectoryAbsoluteFilePath(this));

        //this.setContentView(this.getContentViewId());
        this.setContentView(R.layout.main);

        /*// extract names of samples from res/arrays
        final String[] values = this.getListLabels();

        // use default list-ArrayAdapter *//*
        this.setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, values));
        TextView t = (TextView) findViewById(R.id.textView1);
        String versionInfo = "SDK build date: " + ArchitectView.getBuildProperty("build.date");
        t.setText(versionInfo);
        startMainActivity(0);*/
    }


    /**
     * deletes content of given directory
     *
     * @param path
     */
    private static void deleteDirectoryContent(final String path) {
        try {
            final File dir = new File(path);
            if (dir.exists() && dir.isDirectory()) {
                final String[] children = dir.list();
                for (int i = 0; i < children.length; i++) {
                    new File(dir, children[i]).delete();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static class SampleMeta {

        final String path, categoryName, sampleName, categoryId;
        final int sampleId;
        final boolean hasGeo, hasIr;

        public SampleMeta(String path, boolean hasIr, boolean hasGeo) {
            super();
            this.path = path;
            this.hasGeo = hasGeo;
            this.hasIr = hasIr;
            if (path.indexOf("_") < 0) {
                throw new IllegalArgumentException("all files in asset folder must be folders and define category and subcategory as predefined (with underscore)");
            }
            this.categoryId = path.substring(0, path.indexOf("_"));
            path = path.substring(path.indexOf("_") + 1);
            this.categoryName = path.substring(0, path.indexOf("_"));
            path = path.substring(path.indexOf("_") + 1);
            this.sampleId = Integer.parseInt(path.substring(0, path.indexOf("_")));
            path = path.substring(path.indexOf("_") + 1);
            this.sampleName = path;
        }

        @Override
        public String toString() {
            return "categoryId:" + this.categoryId + ", categoryName:" + this.categoryName + ", sampleId:" + this.sampleId + ", sampleName: " + this.sampleName + ", path: " + this.path;
        }
    }

    /**
     * helper to check if video-drawables are supported by this device. recommended to check before launching ARchitect Worlds with videodrawables
     *
     * @return true if AR.VideoDrawables are supported, false if fallback rendering would apply (= show video fullscreen)
     */
    public static final boolean isVideoDrawablesSupported() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Lollipop: assume it's ok
            // because creating a new GL context only to check this extension is overkill
            return true;
        } else {
            String extensions = GLES20.glGetString(GLES20.GL_EXTENSIONS);
            return extensions != null && extensions.contains("GL_OES_EGL_image_external");
        }
    }


}
