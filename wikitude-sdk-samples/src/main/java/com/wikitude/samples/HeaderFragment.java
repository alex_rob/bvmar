package com.wikitude.samples;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.wikitude.architect.ArchitectView;
import com.wikitude.architect.StartupConfiguration;
import com.wikitude.sdksamples.R;
import com.wikitude.tools.device.features.MissingDeviceFeatures;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by ralexand on 1/22/2017.
 */

public class HeaderFragment extends Fragment {
    TextView output;
    private Map<Integer, List<MainActivity.SampleMeta>> samples;

    private Set<String> irSamples;
    private Set<String> geoSamples;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.header_fragment, viewGroup, false);

        output  = (TextView) view.findViewById(R.id.header);
        output.setText("Device Inventory");

        return view;
    }
}
